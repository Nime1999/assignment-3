#include <stdio.h>
#include <stdlib.h>

int main()
{
    char letter;
    printf("Enter an alphabet :");
    scanf("%c",&letter);
    if (letter =='a'||letter =='e'||letter =='i'||letter =='o'||letter =='u'||letter =='A'||letter =='E'||letter =='I'||letter =='O'||letter =='U')
    {
        printf("%c is a Vowel\n",letter);
    }
    else
    {
        printf("%c is a Consonant\n",letter);
    }
    return 0;
}
